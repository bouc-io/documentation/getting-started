#! /bin/bash


echo 'Installing Istio first... (First part!)'
kubectl apply -f namespace.yaml
istioctl install --set profile=demo -y --set meshConfig.accessLogFile=/dev/stdout
kubectl apply -f local.ingress-gateway.yaml
kubectl apply -f peerauthentication.yaml
kubectl apply -f local.destinationrule.yaml

echo 'Installing Cert-Manager...'
kubectl apply -f namespace.yaml
helm upgrade -install cert-manager jetstack/cert-manager --namespace cert-manager --set installCRDs=true --set prometheus.enabled=false --set webhook.timeoutSeconds=4
kubectl apply -f local.clusterissuer.yaml
kubectl apply -f local.certificate.yaml

echo 'Installing kubernetes-dasbhoard...'
kubectl apply -f namespace.yaml
helm upgrade -install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard -f local.values.yaml -n kubernetes-dashboard
kubectl apply -f dashboard-adminuser.yaml

echo 'Installing Keycloak...'
kubectl apply -f namespace.yaml
helm upgrade -install keycloak-release -f local.values.yaml bitnami/keycloak -n keycloak
echo 'Configure keycloak with proper realm and user configurations...'
echo '1. Load the appropriate realm configuration file (e.g. local.users-realm-export.json)'
echo 'Ready to continue?  Press enter when ready.'
read contrinuetrigger
echo '2.1. Generate a new secret for the Oauth2-proxy client under the new Users realm imported at the previous step'
echo '2.2. Copy the new secret into the config of the Oauth2-proxy module.'
echo 'Ready to continue?  Press enter when ready.'
read contrinuetrigger
echo '3. Within the Users realm, create a new user to use for standard login (e.g. user-1 and user-1 for username and password)'
echo 'Ready to continue?  Press enter when ready.'
read contrinuetrigger


echo 'Installing OAuth2-Proxy...'
kubectl apply -f namespace.yaml
kubectl apply -f redis-credentials-secret.yaml
helm upgrade -install oauth2-proxy-rel -f local.values.yaml bitnami/oauth2-proxy -n oauth2-proxy

echo 'Install Istio first... (Second part! - Authn and Authz are required before...)'
kubectl apply -f serviceentry.yaml
kubectl apply -f egress-gateway.yaml
kubectl apply -f configmap.yaml
kubectl apply -f ext-authz-oauth2-proxy.yaml

