#! /bin/bash

echo 'Installing all toolings...'
brew install git
brew install helm
brew install kubernetes-cli
brew install tfenv
brew install git-flow
brew install node
brew install google-cloud-sdk
brew install direnv

echo 'Setting up the entire GitLab structure for Bouc.io...'

mkdir infrastructure
mkdir application

echo 'Obtaining the infrastruscture code...'
cd ./infrastructure

echo 'Creating and obtaining Istio...'
git clone git@gitlab.com:bouc-io/infrastructure/istio.git
cd istio
curl -L https://istio.io/downloadIstio | sh -
export PATH=$PATH:./istio-1.13.3/bin
cd ../.

echo 'Creating and obtaining Cert-Manager...'
git clone git@gitlab.com:bouc-io/infrastructure/cert-manager.git
helm repo add jetstack https://charts.jetstack.io

echo 'Creating and obtaining kubernetes-dashboard...'
git clone git@gitlab.com:bouc-io/infrastructure/k8s-dashboard.git
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/

echo 'Creating and obtaining Keycloak...'
git clone git@gitlab.com:bouc-io/infrastructure/keycloak.git
helm repo add bitnami https://charts.bitnami.com/bitnami

echo 'Creating and obtaining OAuth2-Proxy...'
git clone git@gitlab.com:bouc-io/infrastructure/oauth2-proxy.git
helm repo add bitnami https://charts.bitnami.com/bitnami

echo 'Obtaining the application code...'
cd ../application
mkdir backend
mkdir frontend

echo 'Obtaining the examples...'
mkdir examples
cd ./examples

mkdir api-example
cd ./api-example
git clone git@gitlab.com:bouc-io/application/examples/api-example/api-chart.git
git clone git@gitlab.com:bouc-io/application/examples/api-example/api-example.git
cd ../.

mkdir static-example
cd ./static-example
git clone git@gitlab.com:bouc-io/application/examples/static-website/static-web-chart.git
git clone git@gitlab.com:bouc-io/application/examples/static-website/static-web-example.git
cd ../.


echo 'Setup complete.'