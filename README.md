# Bouc.io Sandbox Infrastructure Open Source Project

Bouc.io is a personal, highly opinionated, open-source sandbox infrastructure project used for personal development project and learning opportunities.  The technologies stack used includes: GCP with GKE, Bigtable and SQL, GitLab, HashiCorp Terraform, kubernetes, cert-manager leveraging Let’s Encrypt, kubernetes-dashboard, Istio as service mesh and API gateway, keycloak and kiali.  It also includes code references and examples using Nginx (an example for a static website), NodeJS (an example for a secured API) and React (an example for a transactional, SPA website).

![Bouc.io Project](./boucio-img.png)


## Why Bouc.io?

I originally started to work on this personal project on my own time as way to upgrade and maintain my technical skills sharp while learning the latest technologies used within the infrastructure domain at the best tech companies.  Eventually, I decided to create Bouc.io to publish this work under after having been through at my current and last employment the difficulty of putting "all the pieces together", in order to produce a workable, viable infrasctucture.  The learning curve for each technology is quite steep, put together it is even more challenging and while there were a lot of "piece meal" examples out there, there weren't one covering all the basis of typical enterprise infrastructure.

I don't have the pretention to have the "perfect setup" and this should be followed, but it does provide you a setup that works while covering most uses cases for web project nowadays. 

Hopefully you'll find the project useful for your own learning activities!

Enjoy!
- [@MCoteDigital](https://twitter.com/MCoteDigital)


## Getting Started

**IMPORTANT:**
1. Please ensure to install the prerequisites prior to running the scripts.  The scrips aren't really fancy and aren't written to be able to be re-ran without issues.
2. The current setup is entirely done from a Mac and for a Mac (sorry Windows users!).


### Installation

*PREREQUISITES*

Docker Desktop must be installed on your local Mac with the 'kubernetes' option turned on.  Note, for better performance, it is preferrable to increase the CPU and RAM to 8 and 12 repectively.  If you haven't installed Docker yet, please go to [Docker website](https://www.docker.com/products/docker-desktop).

Homebrew is leveraged for the installation of all tools required.  To download and install Homebrew, go to the [Homebrew website](https://brew.sh).


*MAIN INSTALLATION*

Copy the scripts 'init-local-infra.sh' and 'init-local-k8s-docker.sh' located within this ["Getting Started" Git repo](https://bouc.io) to the main folder planned to be used as your workspace; usually something like:
```shell
mkdir ~/bouc_io_wrksp
```

Execute the following command to create the proper folder structure and obtain the code source:
```shell
./init-local-infra.sh
```

Once completed, execute the following command to install all components properly into your Mac:
```shell
./init-local-k8s-docker.sh
```

After the installation is completed, edit your hosts file using the following command:
```shell
sudo vi /etc/hosts
```
And add the following entries:
```shell
## ...for local Kubernetes cluster host
127.0.0.1 docker.internal
127.0.0.1 www.docker.internal
127.0.0.1 api.docker.internal
127.0.0.1 authz.app.docker.internal
127.0.0.1 test.api.docker.internal
127.0.0.1 kubernetes-dashboard.app.docker.internal
127.0.0.1 authn.app.docker.internal
```

You should now be good to go!

To reach the main websites setup, use any of the URLs added to your hosts file.  If asked, the main users to log in are: admin/admin and user-1/user-1 for the username and password respectively.


## References

- [Docker](https://www.docker.com)
- [Homebrew](https://brew.sh)
- [Git](https://git-scm.com)
- [Helm](https://helm.sh)
- [Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [Istio service mesh](https://istio.io)
- [Cert-Manager](https://cert-manager.io)
- [kubernetes-dashboard](https://github.com/kubernetes/dashboard)
- [Keycloak](https://www.keycloak.org)
- [OAuth2-Proxy](https://oauth2-proxy.github.io/oauth2-proxy/)
- [NodeJS](https://nodejs.org/en/)
- [NGinx](https://www.nginx.com)

 
## Project Structure

- **Documentation**: Contains the main documentation to start with the project and install the main components along with the code.
- **Application**: Contains the examples used as references as well as validation.
- **Infrastructure**: Contains all the main code for the infrastructure components such as Istio, Cert-Manager, Keycloak and OAuth2-Proxy.


## License

[Apache License 2.0](https://opensource.org/licenses/Apache-2.0)

----
_Copyright 2022 Martin Cote [@MCoteDigital](https://twitter.com/MCoteDigital) and [@Bouc_io](https://twitter.com/Bouc_io)_
